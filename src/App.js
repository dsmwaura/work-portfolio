import React from "react";
// import logo from "./logo.svg";
import "./App.css";
import Header from "./components/Header";
import About from "./components/About"
import Projects from "./components/Projects"
import TechStack from "./components/TechStack"
import Socials from "./components/Socials"
import {useState,useEffect} from 'react'

function App() {
  
  return (
    <>
    
    <Header/>

    <Socials className="float-nav"/>
    <About/>
    <Projects/>
    <TechStack/>


    </>
  );
}

export default App;
