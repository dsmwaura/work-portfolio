import React from "react";
import Nav from "react-bootstrap/Nav";
import { FcGoogle } from "react-icons/fc";
import  {GrTwitter} from 'react-icons/gr'
import {SiLinkedin} from 'react-icons/si'
import {RiGitlabFill} from 'react-icons/ri'
import {SiWhatsapp} from 'react-icons/si'

const SocialNav = () => {
  return <Nav>

  <Nav.Link className="gmail-float my-float" href="mailto:dsmwaura@gmail.com"><FcGoogle /></Nav.Link>
  <Nav.Link href="https://twitter.com/OmariSalim" className="twitter-float my-float" target="_blank">< GrTwitter /></Nav.Link>
  <Nav.Link  href="https://www.linkedin.com/in/salim-mwaura-682ba736/" className="linkedin-float my-float" target="_blank"><SiLinkedin/></Nav.Link>
  <Nav.Link  href="https://gitlab.com/dsmwaura" className="gitlab-float my-float" target="_blank"n><RiGitlabFill/></Nav.Link>
  <Nav.Link  href="https://api.whatsapp.com/send?phone=254722810063" target="_blank" className="whatsapp-float my-float"><SiWhatsapp/></Nav.Link>
  
</Nav>
};

export default SocialNav;
