import React from "react";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";
// import medpark from "../images/medpark.png";
import "./Projects.css"
import Badge from "react-bootstrap/Badge"; 
import {useState,useEffect} from 'react'
const Projects = () => {
  const [works, setWorks]=useState([])
  useEffect(()=>{
    getProjects().then(r=>{
      console.log(r);
      setWorks(r);
    });

  },[])
  const getProjects = async()=>{
    //let headers ={"Access-Control-Allow-Origin": "*","Content":"application/json"}
    const response = await fetch('/portfolio.json');//https://saleemdev.github.io/portfolio.json
    const payload = await response.json();
    return payload.works; 
  }
  return (
    
    <Container className="container code">
      <h5 id="works" class="code text-center">
        A selection of my <span style={borderBottom}>works</span>{" "}
      </h5>
      <div className="row">
      { works.map((work,idx) => {
        return (
          <Card className="col-sm-2 cardCustom" key={idx} border="success" style={{ width: "18rem" }}>
            <Card.Img variant="top" src={work.img} />
            <Card.Body>
              <Card.Title>{work.title} </Card.Title> 
              <Card.Text>
                <small>
                 {work.text}
                </small>
                <p>Tech Stack :-</p>

                {techBadges(work.tech_stack)}
            
              </Card.Text>
              {/* <Button variant="primary">Go somewhere</Button> */}
              <a
                class="btn btn-primary"
                href={work.link}
                target="_blank"
                rel="noreferrer"
              >
                Link to Resource
              </a>
            </Card.Body>
          </Card>
        );
      })}
      </div>
    </Container>
  );
};

const borderBottom = {
  "border-bottom": "4px solid green",
};

const techBadges = (techStack)=>{
  return techStack.map(stack=>{return <Badge className="badgeMargin" bg="success">{stack}</Badge>}) || "No stack Specified"
}
export default Projects;
