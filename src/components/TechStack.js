import React from "react";
import Badge from "react-bootstrap/Badge";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";
import CardGroup from "react-bootstrap/CardGroup";

const TechStack = () => {
  return (
    <Container
      id="techstack"
      className="border border-success px-4 py-5 my-5 text-center"
    >
      {" "}
      <h5 class="code text-center">
        My Skills and <span style={borderBottom}>tech stack</span>{" "}
      </h5>
      <CardGroup>
        <Card>
          {/* <Card.Img variant="top" src="holder.js/100px160" /> */}
          <Card.Body>
            <Card.Title>Frontend Development</Card.Title>
            <Card.Text>
              <Badge bg="secondary">HTML5</Badge>{" "}
              <Badge bg="secondary">Vanilla JS</Badge>{" "}
              <Badge bg="secondary">CSS3</Badge>{" "}
              <Badge bg="secondary">ReactJS</Badge>{" "}
            </Card.Text>
          </Card.Body>
          <Card.Footer>
            {/* <small className="text-muted">Last updated 3 mins ago</small> */}
          </Card.Footer>
        </Card>
        <Card>
          {/* <Card.Img variant="top" src="holder.js/100px160" /> */}
          <Card.Body>
            <Card.Title>Backend Development</Card.Title>
            <Card.Text>
              <Badge bg="secondary">Python(Django/Flask)</Badge>{" "}
              <Badge bg="secondary">Jinja API</Badge>{" "}
              <Badge bg="secondary">Frappe Framework</Badge>{" "}
              <Badge bg="secondary">ERPNext</Badge>{" "}
            </Card.Text>
          </Card.Body>
          <Card.Footer>
            {/* <small className="text-muted">Last updated 3 mins ago</small> */}
          </Card.Footer>
        </Card>
        <Card>
          {/* <Card.Img variant="top" src="holder.js/100px160" /> */}
          <Card.Body>
            <Card.Title>Solution Oriented Skills</Card.Title>
            <Card.Text>
            <Badge bg="secondary">Git</Badge>{" "}
            <Badge bg="secondary">System Design/Modelling</Badge>{" "}
            <Badge bg="secondary">Agile & RAD with Kanban</Badge>{" "}
            <Badge bg="secondary">Project Management</Badge>{" "}
            </Card.Text>
          </Card.Body>
          <Card.Footer>
            {/* <small className="text-muted">Last updated 3 mins ago</small> */}
          </Card.Footer>
        </Card>
      </CardGroup>
    </Container>
  );
};
const borderBottom = {
  "border-bottom": "4px solid green",
};
export default TechStack;
