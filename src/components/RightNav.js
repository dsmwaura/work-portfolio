import React from "react";
import "../index.css";
import Socials from './Socials'

const RightNav = () => {
  return <div className="right-nav" id="right-nav">
      <Socials />
  </div>;
};

export default RightNav;
