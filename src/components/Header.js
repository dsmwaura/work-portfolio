import React from "react";

import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { BiMap } from "react-icons/bi";
import { DiTerminal } from "react-icons/di";

import Button from "react-bootstrap/Button";
import "../index.css";
import { useState } from "react";
import SocialNav from "./SocialNav";

const Header = () => {
  const [showLinks, setShowLinks] = useState(false);
  return (
    <>
      <Navbar bg="light" sticky="top" collapseOnSelect expand="lg">
        <Container className="code">
          <h1 style={{ "font-size": "80px" }}>
            <DiTerminal />
          </h1>
          {/* <img src={devPhoto} alt ="An illustration of the Dev in question" style={imgProps}/> */}
          <Navbar.Brand id="myname" href="/">
            Salim Mwaura{" "}
          </Navbar.Brand>

          <small>
            <BiMap /> Eldoret Kenya
          </small>
          <Nav className="me-auto" position="right">
            {/* <Nav.Link href="#home">About Me</Nav.Link>
            <Nav.Link href="#about">My Works</Nav.Link>
            <Nav.Link href="#projects">Resume</Nav.Link> */}
          </Nav>
          <Nav>
            <Nav.Link href="#works" style={borderBottom}>
              WORKS
            </Nav.Link>
            <Nav.Link href="#techstack">SKILLS</Nav.Link>
            <Nav.Link href="#about">RESUME</Nav.Link>
            <Button
              className={
                !showLinks
                  ? "btn btn-outline-success"
                  : "btn btn-outline-danger"
              }
              id="float-link"
              onClick={() => setShowLinks(!showLinks)}
            >
              {showLinks ? "CLOSE LINKS" : "CONNECT"}
            </Button>
          </Nav>
        </Container>
      </Navbar>
      {showLinks && <SocialNav style={{ opacity: "0.9" }} />}

      
    </>
  );
};

const borderBottom = {
  "border-bottom": "1px solid green",
};
export default Header;
