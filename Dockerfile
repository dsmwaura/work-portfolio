FROM node:16-alpine

RUN npm install -g nodemon 

RUN npm install -g concurrently 

WORKDIR /work-portfolio

COPY package.json .

RUN yarn


COPY . .

EXPOSE 3000

CMD ["npm","run","dev"]

